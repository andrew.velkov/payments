import React from 'react';
import PropTypes from 'prop-types';

import css from 'style/components/ShoppingCart';

const ShoppingCart = ({ paymentMethod, totals }) => {
  return (
    <div className={ css.totals }>
      <ul className={ css.totals__list }>
        <li className={ css.totals__item }>
          Subtotal <span>$ { totals.subtotal }</span>
        </li>

        {paymentMethod === 'PayPal' && <ul>
          <li className={ css.totals__item }>Payment processing services 1% <span>$ { totals.prs }</span></li>
          <li className={ css.totals__item }>VAT 21% <span>$ { totals.vat }</span></li>
        </ul>}

        {paymentMethod === 'Visa' && <li className={ css.totals__item }>
          Payment processing services 1%
          <span>$ { totals.prs }</span>
        </li>}
      </ul>

      <h4 className={ css.totals__footer }>
        Total <span>$ { totals.total }</span>
      </h4>
    </div>
  );
};

ShoppingCart.propTypes = {
  paymentMethod: PropTypes.string,
  totals: PropTypes.object,
};

export default ShoppingCart;
