import React from 'react';
import PropTypes from 'prop-types';

import css from 'style/components/ShoppingCart';

const ShoppingCartList = ({ data }) => (
  <ul className={ css.cartList }>
    {data.map(product => {
      return (
        <li key={ product.id } className={ css.cartList__item }>
          <img className={ css.cartList__img } src={ product.image } alt='' />
          <div className={ css.cartList__info }>
            <h4>{ product.title }</h4>
            <p>{ product.designer }</p>
            <p>$ <span>{ product.price }</span></p>
          </div>
        </li>
      );
    })}
  </ul>
);

ShoppingCartList.propTypes = {
  data: PropTypes.any,
};

export default ShoppingCartList;
