import React from 'react';
import PropTypes from 'prop-types';
import { Dialog } from 'material-ui';
import { Link } from 'react-router-dom';

import Button from 'components/Button';

import css from 'style/components/Dialog';

const styles = {
  button: {
    margin: '0px 12px 12px',
    marginTop: 0,
  },
};

export default class DialogExample extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    buttonSend: PropTypes.bool,
    buttonDelete: PropTypes.bool,
    disabled: PropTypes.bool,
    open: PropTypes.bool,
    preview: PropTypes.func,
    onClick: PropTypes.func,
    handleDelete: PropTypes.func,
    onDoubleClick: PropTypes.func,
    title: PropTypes.string,
    buttonType: PropTypes.string,
    buttonName: PropTypes.string,
    className: PropTypes.string,
  }

  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { buttonSend = true, buttonDelete = false, buttonType = 'button', handleDelete, title, preview, onClick, onDoubleClick, className, disabled, buttonName, children, ...params } = this.props;
    const actions = [
      <span className={ css.dialogButtons }>
        {buttonSend && <Button
          label='Save'
          primary={ true }
          disabled={ disabled }
          onClick={ onClick }
        />}
        {buttonDelete && <Button
          label='Delete'
          secondary={ true }
          disabled={ disabled }
          onClick={ handleDelete }
        />}
      </span>,
      <Button
        typeButton='flat'
        style={ styles.button }
        label='Cancel'
        onClick={ this.handleClose }
      />,
    ];

    return (
      <span>
        {buttonType === 'link' &&
          <Link to='#' className={ className } onDoubleClick={ this.handleOpen } onClick={ preview } { ...params }>{ buttonName }</Link>
        }

        {buttonType === 'button' &&
          <Button onClick={ this.handleOpen } { ...params }>{ buttonName }</Button>
        }

        {buttonType === 'icon' &&
          <Button typeButton='icon' iconName={ buttonName } onClick={ this.handleOpen } { ...params } />
        }

        {buttonType === 'circle' &&
          <Button typeButton='circle' iconName={ buttonName } onClick={ this.handleOpen } { ...params } />
        }

        {this.state.open && <Dialog
          title={ title }
          actions={ actions }
          modal={ false }
          open={ this.state.open }
          contentClassName={ css.dialog }
          actionsContainerClassName={ css.dialog__actions }
          onRequestClose={ this.handleClose }
        >
          { children }
        </Dialog>}
      </span>
    );
  }
}
