import React from 'react';
import PropTypes from 'prop-types';

import Radio from 'components/Form/Radio';

import css from 'style/pages/Main';

const PaymentSelection = ({ data, valueSelected, onChange }) => {
  return (
    <div className={ css.stepperContent__body }>
      <h4 className={ css.stepperContent__title }>Payment Selection</h4>
      <Radio data={ data } name='paymentMethod' valueSelected={ valueSelected } onChange={ onChange } />
    </div>
  );
};

PaymentSelection.propTypes = {
  data: PropTypes.array,
  valueSelected: PropTypes.string,
  onChange: PropTypes.func,
};

export default PaymentSelection;
