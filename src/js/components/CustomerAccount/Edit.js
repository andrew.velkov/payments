import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getAccounts, editAccount, removeAccount } from 'actions';
import Dialog from 'components/Dialog';
import Form from 'components/CustomerAccount/Form';

@connect(() => ({}), { getAccounts, editAccount, removeAccount })
export default class CustomerAccountEdit extends Component {
  static propTypes = {
    data: PropTypes.object,
    editAccount: PropTypes.func,
    getAccounts: PropTypes.func,
    removeAccount: PropTypes.func,
  };

  constructor(props) {
    super(props);
    const { name, address, payments } = this.props.data;

    this.state = {
      name,
      address,
      method: payments.method,
      card: payments.card,
      expiryDate: payments.expiryDate,
      cvv: payments.cvv,
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleChangeSelect = (e, index, data) => {
    this.setState({
      method: data.value,
    });

    if (this.state.method) {
      this.setState({ card: '' });
    }
  }

  handleSubmit = async (e, id) => {
    e.preventDefault();
    const { name, address, method, card, expiryDate, cvv } = this.state;
    const data = {
      address,
      name,
      payments: {
        method,
        card,
        expiryDate: method === 'PayPal' ? '' : expiryDate,
        cvv: method === 'PayPal' ? '' : cvv,
      },
    };

    await this.props.editAccount(id, data);
    this.props.getAccounts();
  }

  handleDeleteAccount = async (e, id) => {
    e.preventDefault();

    await this.props.removeAccount(id);
    this.props.getAccounts();
  }

  render() {
    const { data: { id } } = this.props;
    const { name, address, method, card, expiryDate, cvv } = this.state;

    return (
      <Dialog
        title='Edit Customer Account'
        buttonType='icon'
        buttonName='more_vert'
        buttonDelete={ true }
        secondary={ true }
        onClick={ (e) => this.handleSubmit(e, id) }
        handleDelete={ (e) => this.handleDeleteAccount(e, id) }
      >
        <section>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <Form data={ { name, address, method, card, expiryDate, cvv } } handleChange={ this.handleChange } handleChangeSelect={ this.handleChangeSelect } />
        </section>
      </Dialog>
    );
  }
}
