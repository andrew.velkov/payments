import React from 'react';
import PropTypes from 'prop-types';

import AddCustomer from 'components/CustomerAccount/Add';
import EditCustomer from 'components/CustomerAccount/Edit';

import css from 'style/components/CustomerAccount';

const CustomerAccount = ({ data, customers, onChange }) => (
  <div className={ css.stepperContent__body }>
    <h4 className={ css.stepperContent__title }>Customer Account</h4>

    <ul className={ css.customerList }>
      {data.map(item => {
        return (
          <li key={ item.id } className={ css.customerList__item }>
            <div className={ css.customerGroup }>
              <input
                className={ css.customerGroup__input }
                id={ `account_${ item.id }` }
                type='radio'
                name='customers'
                value={ item.id }
                defaultChecked={ customers === item.id }
                onChange={ onChange }
              />
              <label className={ css.customerGroup__label } htmlFor={ `account_${ item.id }` }>
                <h5>{ item.name } / { item.payments.method }</h5>
                <p>Address: { item.address }</p>
                <p>Account No.: { item.payments.card }</p>
                {item.payments.method === 'Credit Card' && <div>
                  <p>Expiry Date { item.payments.expiryDate }</p>
                  <p>CVV { item.payments.cvv }</p>
                </div>}
                <div className={ css.customerGroup__actions }>
                  <EditCustomer data={ item } />
                </div>
              </label>
            </div>
          </li>
        );
      })}
      <li className={ css.customerList__button }>
        <AddCustomer />
      </li>
    </ul>
  </div>
);

CustomerAccount.propTypes = {
  data: PropTypes.any,
  onChange: PropTypes.func,
  customers: PropTypes.number,
};

export default CustomerAccount;
