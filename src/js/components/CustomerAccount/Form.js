import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Input from 'components/Form/Input';
import Select from 'components/Form/Select';

import paymentMethodConfig from 'config/paymentMethod';

import css from 'style/components/CustomerAccount';

const Form = ({ data, handleChange, handleChangeSelect }) => (
  <form>
    <Input label='Name' name='name' value={ data.name } onChange={ handleChange } />
    <Input label='Address' name='address' value={ data.address } onChange={ handleChange } multiLine={ true } />
    <Select data={ paymentMethodConfig } label='Payment Method' value={ data.method } handleChange={ handleChangeSelect } />

    {data.method === 'Credit Card' && <div>
      <p>Safe money transfer using your bank account. Visa, Maestro, Discover, American Express.</p>
      <ul className={ cx(css.formGroup, css.formGroup_card) }>
        <li className={ css.formGroup__item }>
          <Input label='Card number' hintText='0000 0000 0000 0000' name='card' value={ data.card } onChange={ handleChange } />
        </li>
        <li className={ css.formGroup__item }>
          <Input label='Expiry date' hintText='MM / YY' name='expiryDate' value={ data.expiryDate } onChange={ handleChange } />
        </li>
        <li className={ css.formGroup__item }>
          <Input label='CVV code' hintText='000' name='cvv' value={ data.cvv } onChange={ handleChange } />
        </li>
      </ul>
    </div>}

    {data.method === 'PayPal' && <Input label='Email address' name='card' value={ data.card } onChange={ handleChange } />}
  </form>
);

Form.propTypes = {
  data: PropTypes.any,
  handleChange: PropTypes.func,
  handleChangeSelect: PropTypes.func,
};

export default Form;
