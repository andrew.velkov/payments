import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { createAccount, getAccounts } from 'actions';
import Dialog from 'components/Dialog';
import Form from 'components/CustomerAccount/Form';

@connect(() => ({}), { createAccount, getAccounts })
export default class CustomerAccountAdd extends Component {
  static propTypes = {
    createAccount: PropTypes.func,
    getAccounts: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      address: '',
      method: null,
      card: '',
      expiryDate: '',
      cvv: '',
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleChangeSelect = (e, index, data) => {
    this.setState({
      method: data.value,
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const { address, name, method, card, expiryDate, cvv } = this.state;
    const data = { address, name, payments: { method, card, expiryDate, cvv } };

    await this.props.createAccount(data);
    this.props.getAccounts();

    this.setState({
      name: '',
      address: '',
      method: null,
      card: '',
      expiryDate: '',
      cvv: '',
    });
  }

  render() {
    const { name, address, method, card, expiryDate, cvv } = this.state;

    return (
      <Dialog
        title='Add Customer Account'
        buttonType='circle'
        buttonName='add'
        secondary={ true }
        onClick={ (e) => this.handleSubmit(e) }
      >
        <section>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <Form data={ { name, address, method, card, expiryDate, cvv } } handleChange={ this.handleChange } handleChangeSelect={ this.handleChangeSelect } />
        </section>
      </Dialog>
    );
  }
}
