import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import css from 'style/components/Stepper';

const StepperList = ({ stepsNavConfig, stepIndex }) => (
  <ul className={ css.stepperNav }>
    {stepsNavConfig.map((item, index) => {
      return (
        <li key={ item.id } className={ cx(css.stepperNav__item, { [css.stepperNav__item_active]: index <= stepIndex }) }>
          { index }. { item.value }
        </li>
      );
    })}
  </ul>
);

StepperList.propTypes = {
  stepsNavConfig: PropTypes.array,
  stepIndex: PropTypes.number,
};

export default StepperList;
