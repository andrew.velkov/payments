import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Button from 'components/Button';

import css from 'style/components/Stepper';

const StepperButtons = ({ stepsNavConfig, stepIndex, finished, onClickPrev, onClickNext }) => (
  <div className={ cx(css.buttonGroup, css.buttonGroup_between) }>
    <div className={ css.buttonGroup__item }>
      <Button typeButton='flat' disabled={ stepIndex === 0 } onClick={ onClickPrev }>
        Return to Shop
      </Button>
    </div>
    <div className={ css.buttonGroup__item }>
      {!finished ?
        <Button primary={ true } onClick={ onClickNext }>
          {stepIndex === stepsNavConfig.length - 1 ? 'Complete order' : 'Continue to payment' }
        </Button>
        :
        <Button type='submit' secondary={ true }>
          Complete order
        </Button>
      }
    </div>
  </div>
);

StepperButtons.propTypes = {
  finished: PropTypes.bool,
  stepsNavConfig: PropTypes.any,
  stepIndex: PropTypes.number,
  onClickNext: PropTypes.func,
  onClickPrev: PropTypes.func,
};

export default StepperButtons;
