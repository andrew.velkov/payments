import * as type from 'constants/accounts';

const initialState = {
  get: {
    loaded: false,
    loading: false,
    customerAccountData: [],
  },
  create: {
    loaded: false,
    loading: false,
  },
  edit: {
    loaded: false,
    loading: false,
  },
  remove: {
    loaded: false,
    loading: false,
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case type.GET_ACCOUNT:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case type.GET_ACCOUNT_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          customerAccountData: action.payload.result.data,
        },
      };
    case type.GET_ACCOUNT_ERROR:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: false,
        },
      };
    case type.CREATE_ACCOUNT:
      return {
        ...state,
        create: {
          loading: true,
          loaded: false,
        },
      };
    case type.CREATE_ACCOUNT_SUCCESS:
      return {
        ...state,
        create: {
          loading: false,
          loaded: true,
        },
      };
    case type.CREATE_ACCOUNT_ERROR:
      return {
        ...state,
        create: {
          loading: false,
        },
      };
    case type.EDIT_ACCOUNT:
      return {
        ...state,
        edit: {
          loading: true,
          loaded: false,
        },
      };
    case type.EDIT_ACCOUNT_SUCCESS:
      return {
        ...state,
        edit: {
          loading: false,
          loaded: true,
        },
      };
    case type.EDIT_ACCOUNT_ERROR:
      return {
        ...state,
        edit: {
          loading: false,
        },
      };
    case type.REMOVE_ACCOUNT:
      return {
        ...state,
        remove: {
          loading: true,
          loaded: false,
        },
      };
    case type.REMOVE_ACCOUNT_SUCCESS:
      return {
        ...state,
        remove: {
          loading: false,
          loaded: true,
        },
      };
    case type.REMOVE_ACCOUNT_ERROR:
      return {
        ...state,
        remove: {
          loading: false,
        },
      };
    default:
      return state;
  }
}
