import { combineReducers } from 'redux';

import products from './products';
import accounts from './accounts';

const reducers = combineReducers({
  products,
  accounts,
});

export default reducers;
