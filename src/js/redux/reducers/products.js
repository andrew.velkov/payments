import * as type from 'constants/products';

const initialState = {
  get: {
    loaded: false,
    loading: false,
    data: [],
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case type.GET_PRODUCTS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case type.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          data: action.payload.result.data,
        },
      };
    case type.GET_PRODUCTS_ERROR:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: false,
        },
      };
    default:
      return state;
  }
}
