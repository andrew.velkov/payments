const paymentMethod = [
  {
    id: 1,
    value: 'Credit Card',
  },
  {
    id: 2,
    value: 'PayPal',
  },
];

export default paymentMethod;
