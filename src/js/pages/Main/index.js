import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { sumBy } from 'lodash';

import { getProducts, getAccounts } from 'actions';

import Fetching from 'components/Fetching';
import CustomerAccount from 'components/CustomerAccount';
import PaymentSelection from 'components/PaymentSelection';
import ShoppingCartList from 'components/ShoppingCart/List';
import ShoppingCartTotals from 'components/ShoppingCart/Totals';
import StepperList from 'components/Stepper/List';
import StepperButtonGroup from 'components/Stepper/ButtonGroup';

import paymentSelectionConfig from 'config/paymentSelection';
import stepsNavConfig from 'config/stepsNav';

import css from 'style/pages/Main';

@connect(state => ({
  accounts: state.accounts.get,
  products: state.products.get,
}),
{ getProducts, getAccounts }
)
export default class Main extends Component {
  static propTypes = {
    accounts: PropTypes.object,
    products: PropTypes.object,
    getAccounts: PropTypes.func,
    getProducts: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      finished: false,
      stepIndex: 0,
      customers: 1,
      paymentMethod: paymentSelectionConfig[0].value,
    };
  }

  componentDidMount() {
    this.props.getProducts();
    this.props.getAccounts();
  }

  getStepContent = (stepIndex) => {
    switch (stepIndex) {
      case 0:
        return this.customerAccount();
      case 1:
        return this.paymentSelection();
      case 2:
        return this.paymentConfirmation();
      default:
        return 'Error';
    }
  }

  getPercent = (value, percent) => ((value * percent) / 100);

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleNext = () => {
    const { stepIndex } = this.state;

    this.setState({
      stepIndex: stepIndex + 1,
      finished: stepIndex >= stepsNavConfig.length - 1,
    });
  };

  handlePrev = () => {
    const { stepIndex } = this.state;

    if (stepIndex > 0) {
      this.setState((prevState) => ({
        stepIndex: prevState.stepIndex - 1,
      }));
    }
  };

  customerAccount = () => {
    const { customerAccountData, loading } = this.props.accounts;
    const { customers } = this.state;

    return (
      <Fetching isFetching={ loading }>
        <CustomerAccount
          data={ customerAccountData }
          customers={ +customers }
          onChange={ (e) => this.handleChange(e) }
        />
      </Fetching>
    );
  }

  paymentSelection = () => {
    const { paymentMethod } = this.state;

    return (
      <PaymentSelection
        data={ paymentSelectionConfig }
        valueSelected={ paymentMethod }
        onChange={ (e) => this.handleChange(e) }
      />
    );
  }

  paymentConfirmation = () => {
    const { paymentMethod } = this.state;

    return (
      <div className={ css.stepperContent__body }>
        <h4 className={ css.stepperContent__title }>Confirmation</h4>
        <p>Payment Method: { paymentMethod }</p>
        <i>Are you sure?</i>
      </div>
    );
  }

  handleSubmit = async (e, total) => {
    e.preventDefault();

    const dataPost = { ...this.state, total };
    console.log('data', dataPost);

    this.setState({
      finished: false,
      stepIndex: 0,
      customers: 1,
      paymentMethod: paymentSelectionConfig[0].value,
    });
  }

  render() {
    const { data, loading } = this.props.products;
    const { finished, stepIndex, paymentMethod } = this.state;

    const subtotal = sumBy(data, (payment) => payment.price);
    const prs = this.getPercent(subtotal, 1);
    const vat = this.getPercent(subtotal, 21);
    let total;

    if (paymentMethod === 'PayPal') {
      total = prs + vat + subtotal;
    } else if (paymentMethod === 'Visa') {
      total = prs + subtotal;
    } else {
      total = subtotal;
    }

    return (
      <section className={ css.wrap }>
        <div className={ css.wrap__content }>
          <h2>Payments</h2>
          <div className={ css.grid }>

            <div className={ css.grid__item }>
              <section className={ css.stepper }>
                <div className={ css.stepper__header }>
                  <h3 className={ css.stepper__title }>Steps receiving payments</h3>
                  <StepperList
                    stepsNavConfig={ stepsNavConfig }
                    stepIndex={ stepIndex }
                  />
                </div>

                <div className={ css.stepper__content }>
                  <form className={ css.stepper__form } onSubmit={ (e) => this.handleSubmit(e, total) }>
                    { this.getStepContent(stepIndex) }
                    <StepperButtonGroup
                      stepsNavConfig={ stepsNavConfig }
                      stepIndex={ stepIndex }
                      finished={ finished }
                      onClickPrev={ this.handlePrev }
                      onClickNext={ this.handleNext }
                    />
                  </form>
                </div>
              </section>
            </div>

            <div className={ css.grid__item }>
              <h3 className={ css.grid__title }>
                Shopping Cart <span>({ !loading && data.length })</span>
              </h3>
              <Fetching isFetching={ loading }>
                <ShoppingCartList data={ data } />
                <ShoppingCartTotals
                  paymentMethod={ paymentMethod }
                  totals={ { subtotal, prs, vat, total } }
                />
              </Fetching>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
