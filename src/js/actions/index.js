import { getAccounts, createAccount, editAccount, removeAccount } from './accounts';
import { getProducts } from './products';

export {
  getAccounts,
  getProducts,
  createAccount,
  editAccount,
  removeAccount,
};
