import * as type from 'constants/accounts';

export const getAccounts = () => {
  return {
    types: [type.GET_ACCOUNT, type.GET_ACCOUNT_SUCCESS, type.GET_ACCOUNT_ERROR],
    request: {
      method: 'GET',
      url: '/api/accounts',
    },
  };
};

export const createAccount = data => {
  return {
    types: [type.CREATE_ACCOUNT, type.CREATE_ACCOUNT_SUCCESS, type.CREATE_ACCOUNT_ERROR],
    request: {
      method: 'POST',
      url: '/api/accounts',
      body: data,
    },
  };
};

export const editAccount = (id, data) => {
  return {
    types: [type.EDIT_ACCOUNT, type.EDIT_ACCOUNT_SUCCESS, type.EDIT_ACCOUNT_ERROR],
    request: {
      method: 'PUT',
      url: `/api/accounts/${ id }`,
      body: data,
    },
  };
};

export const removeAccount = id => {
  return {
    types: [type.REMOVE_ACCOUNT, type.REMOVE_ACCOUNT_SUCCESS, type.REMOVE_ACCOUNT_ERROR],
    request: {
      method: 'DELETE',
      url: `/api/accounts/${ id }`,
    },
  };
};
