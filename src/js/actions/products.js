import * as type from 'constants/products';

export const getProducts = () => {
  return {
    types: [type.GET_PRODUCTS, type.GET_PRODUCTS_SUCCESS, type.GET_PRODUCTS_ERROR],
    request: {
      method: 'GET',
      url: '/api/products',
    },
  };
};
