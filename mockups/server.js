const jsonServer = require('json-server');
const server = jsonServer.create();
const path = require('path');
const defaultRoutes = require('./routes.js');
const router = jsonServer.router(path.join(__dirname, 'db.json'));
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(jsonServer.bodyParser);

server.use((req, res, next) => {
  if (req.method === 'PATCH' || req.method === 'PUT') {
    delete req.body.id;
  }

  next();
});

router.render = (req, res) => {
    res.jsonp({
      "status": "success",
      "success": {
        "code": 200
      },
      "result": {
        "data": res.locals.data,
      }
    });
};

// Use default router
server.use(jsonServer.rewriter(defaultRoutes));

//Use default database
server.use(router);

server.listen(3013, () => {
  console.log('JSON Server is running')
});
