# Payments

![alt text](http://i.piccy.info/i9/5f90a47e2cf5b34139b824b7a400d71e/1539396544/70722/1273571/Screenshot_2.png)
![alt text](http://i.piccy.info/i9/d5fb135a294b1afa847d4b90a02c671a/1539396594/67876/1273571/Screenshot_1.png)

```
$ git clone https://gitlab.com/andrew.velkov/payments.git
```

```
$ cd payments
```

```
$ yarn install
```

For start server with mockups:
```
$ yarn start:mock
```

Visit `http://localhost:3003/` 